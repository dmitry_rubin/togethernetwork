$(document).ready(function(){
	
	$('ul.tabs li').click(function(){
		var tab_data = $(this).attr('data-tab');

		$('ul.tabs li').removeClass('current');
		$('.tabs-content').removeClass('current');

		$(this).addClass('current');
		$("#"+tab_data).addClass('current');
	})

})