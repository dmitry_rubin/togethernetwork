'use strict'

var gulp = require('gulp');
var autoprefixer = require('gulp-autoprefixer');
var cssmin = require('gulp-cssmin');
var concat = require('gulp-concat-css');
var less = require('gulp-less');
var rename = require("gulp-rename");
var watch = require('gulp-watch');
var browserSync = require('browser-sync').create();
var imagemin = require('gulp-imagemin');
var spritesmith = require('gulp.spritesmith');
var plumber = require('gulp-plumber');
var uglify = require('gulp-uglify');
//var rigger = require('gulp-rigger');
var flatten = require('gulp-flatten');
var concat = require('gulp-concat');

gulp.task('serve', function() {
  browserSync.init({
    server: "./dist"
  });
  gulp.watch(["app/js/*.js"], ['js']);
  gulp.watch(["app/fonts/*"], ['fonts']);
  gulp.watch(["app/less/*.+(less|css)"], ['less']);
  gulp.watch(["app/template/*.html"], ['html']);
  gulp.watch(['dist/*.html']).on('change', browserSync.reload);
});

gulp.task('less', function () {
  return gulp.src('app/less/main.less')   
    .pipe(plumber())
    .pipe(less())   
    .pipe(autoprefixer({
      browsers: ['last 30 versions'],
      cascade: false
    }))
    .pipe(concat('common.css'))  
    .pipe(cssmin())                       
    .pipe(rename('main.min.css'))
    .pipe(plumber.stop())          
    .pipe(gulp.dest('dist/style'))
    .pipe(browserSync.stream());
});

gulp.task('js', function () {
  gulp.src(['app/js/jquery.js','app/js/jquery-ui.js','app/js/slider.js','app/js/tabs.js'])
  .pipe(plumber())
  .pipe(uglify())
  .pipe(concat('common.js'))
  .pipe(rename('common.min.js'))
  .pipe(plumber.stop())     
  .pipe(gulp.dest('dist/js'))     
  .pipe(browserSync.stream());
});

gulp.task('fonts', function(){
  gulp.src('app/font/*.{ttf,woff2,woff,eot,svg}')
    .pipe(flatten())
   .pipe(gulp.dest('dist/font'));
});

/*gulp.task('html', function () {
  gulp.src('app/template/*.html') 
    .pipe(rigger()) 
    .pipe(gulp.dest('dist'))
    .pipe(browserSync.stream());
});*/

gulp.task('sprite', function () {
  var spriteData = gulp.src('app/images/sprite/*.png').pipe(spritesmith({
    imgName: 'sprite.png',
    cssName: 'sprite.css',
    imgPath: '../images/' + 'sprite.png',
    padding: 20
  }));
  spriteData.img.pipe(gulp.dest('dist/images'));
  spriteData.css.pipe(gulp.dest('app/less'));
});

gulp.task('imagemin', function () {
  gulp.src('app/images/*.+(jpg|png)')
    .pipe(imagemin())
    .pipe(gulp.dest('dist/images'))
});
 
gulp.task('default', [/*'html',*/ 'fonts', 'imagemin', 'sprite', 'less', 'js', 'serve']);